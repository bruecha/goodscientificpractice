# This is a hack for:
# https://github.com/pypa/pip/issues/7953

import setuptools
import site
import sys

site.ENABLE_USER_SITE = "--user" in sys.argv[1:]

if __name__ == "__main__":
    setuptools.setup(
        setuptools_git_versioning={
            "enabled": True,
            "template": "{tag}.rc+git.{sha}",
            # "template": "{tag}.post{ccount}+git.{full_sha}",
            "dirty_template": "{tag}.post{ccount}+git.{sha}+dirty",
            "dev_template": "{tag}.post{ccount}+git.{sha}",
        },
        setup_requires=["setuptools-git-versioning<2"],
    )
