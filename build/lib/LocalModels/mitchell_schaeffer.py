import numpy as np


def mitchell_schaeffer(t_in, t_out, t_open, t_close, v_gate, j_stim):
    """Returns a callable to calculate the next Mitchell Schaeffer
    timestep.
    Args:
        - t_in, t_out, t_open, t_close, v_gate: Model Parameter
        - j_stim: callable of form j_stim(t) that describes stimulus current at
        the point in time. if no stimulus current applied pass e.g.
        `j_stim = lambda t: 0` .
    Returns:
        - ms_state_derivative: callable of form
        dstate_vector/dt = ms_equation(state_vector, t)
        Gives the change of the state dstate_vector/dt.
    """

    def j_in(v, h):
        return h * (v**2 * (1 - v)) / t_in

    def j_out(v):
        return -v / t_out

    def dh_dt(v, h):
        return np.where(v < v_gate, (1 - h) / t_open, -h / t_close)

    def state_derivative(state, t):
        """"""
        v, h = state[0], state[1]
        # dv/dt in the paper, my notation: du/dt
        dv = j_in(v, h) + j_out(v) + j_stim(t)
        dh = dh_dt(v, h)
        return np.stack([dv, dh])

    return state_derivative
