def mitchell_schaeffer():
    """From paper of mitchell schaeffer.
    Args:
        -
    Returns dictionary with keys:
        't_in", "t_out", "t_open", "t_close", "v_gate"
    """
    return {"t_in": 0.3, "t_out": 6, "t_open": 120, "t_close": 150, "v_gate": 0.13}
