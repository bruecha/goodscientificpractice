import GoodScientificPractice as gsp


def pre_activity():
    my_activity = gsp.ActivityDocumentation(
        realisation_name="",
        activity_description="",
        activity_output_folder="path/to/folder",
        overwrite_output_folder=False,
    )
    my_activity.load_parameter_file(
        "path_to_parameter", format="standard", read_function=None
    )
    var_name = my_activity.log_and_load(
        loading_function, *loading_function_args, **loading_function_kwargs
    )
    my_activity.add_output(file_name, saving_function, to_output_dir=True)


def activity():
    """
    ...
    Activity functions
    """


def post_activity():
    my_activity.write_documentation()
    pass


def main():
    post_activity(activity(pre_activity()))


if __name__ == "__main__":
    main()
