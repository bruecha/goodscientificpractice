#!/usr/bin/env python3
# ...

import pytest  # noqa: F401
from GoodScientificPractice.Parameters import ParameterSet
import GoodScientificPractice as gsp
from tempfile import NamedTemporaryFile
import numpy as np
import os


def test_parameter_set_is_in_parameter_collection():
    root_set = ParameterSet()
    set_to_find = ParameterSet(a=1, b=2, subset=ParameterSet(c=4))
    test_set_empty = ParameterSet()
    test_set_full = ParameterSet(a=1, b=2)
    test_set_with_set_to_find = ParameterSet(a=1, b=set_to_find)

    assert set_to_find._is_part_of_collection(root_set) is False

    root_set.test_set = test_set_empty
    assert set_to_find._is_part_of_collection(root_set) is False

    root_set.test_set = test_set_full
    assert set_to_find._is_part_of_collection(root_set) is False

    root_set.test_set2 = test_set_with_set_to_find
    assert set_to_find._is_part_of_collection(root_set) is True

    root_set.test_set2 = test_set_full
    assert set_to_find._is_part_of_collection(root_set) is False

    test_set_full.test_set = test_set_with_set_to_find
    assert set_to_find._is_part_of_collection(root_set) is True

    root_set.test = test_set_full
    root_set.test.test = test_set_full
    root_set.test.test = test_set_full
    assert set_to_find._is_part_of_collection(root_set) is True


def test_parameter_dump():
    output_yaml = "\n".join(
        [
            "a: 4",
            "added_set:",
            "  a: 4",
            "  added_child_set:",
            "    a: 4",
            "    blue: blue",
            "    your_mom: beer",
            "  blue: blue",
            "  your_mom: beer",
            "blue: blue",
            "your_mom: beer",
            "",
        ]
    )
    param_set = _setup_nocaosdb_parameter_set()
    assert str(param_set) == output_yaml


def test_activity_documentation():
    output_dir = "./unittests/test_output/no_caosdb"
    _clean_output_dir(output_dir)
    archive = gsp.ActivityDocumentation(
        activity_description="This is a test for the activity documentation class",
        activity_output_folder="./unittests/test_output/no_caosdb",
        overwrite_output_folder=True,
    )
    fp = "./unittests/TestInput/arr.npy"
    arr = archive.inputs.log_and_load(fp, np.load, fp)
    archive.parameters.add_parameters(blue="blue", a=4, your_mom="beer")
    added_set1 = archive.parameters.create_and_add_parameter_set(
        "added_set", blue="blue", a=4, your_mom="beer"
    )
    added_set1.create_and_add_parameter_set(
        "added_child_set", blue="blue", a=4, your_mom="beer"
    )
    archive.outputs.add_output("arr.npy", lambda fp: np.save(fp, arr))
    archive.single_value_results.add_value("cleopatra", "ancient egypt")

    archive.write_documentation()
    assert os.path.isfile("./unittests/test_output/no_caosdb/Inputs.txt")
    assert os.path.isfile("./unittests/test_output/no_caosdb/Parameters.yaml")
    assert os.path.isfile("./unittests/test_output/no_caosdb/Readme.md")
    assert os.path.isfile("./unittests/test_output/no_caosdb/SingleValueResults.yaml")
    assert os.path.isfile("./unittests/test_output/no_caosdb/arr.npy")
    assert os.path.isfile("./unittests/test_output/no_caosdb/environment.yaml")
    assert os.path.isfile("./unittests/test_output/no_caosdb/output.info")


def test_constructions():
    dict_ = dict(
        a=4,
        blue="blue",
        your_mom="beer",
        added_child_set=dict(a=4, blue="blue", your_mom="beer"),
        added_set=dict(a=4, blue="blue", your_mom="beer"),
    )
    params = ParameterSet()
    params.construct_from_dict(dict_)
    assert (
        params.construct_from_dict(
            params._convert_to_dict_tree()
        )._convert_to_dict_tree()
        == dict_
    )

    r = _setup_caosdb_record()
    r_loaded = _setup_caosdb_record(empty_record=True)

    with NamedTemporaryFile("w") as f:
        f.write(str(r))
        f.seek(0)
        r_loaded = r_loaded.construct_from_yaml(f.name)

    assert str(r) == str(r_loaded)


def test_nocaosdb_to_dict():
    param_set = _setup_nocaosdb_parameter_set()
    param_set_as_dict = {
        "blue": "blue",
        "a": 4,
        "your_mom": "beer",
        "added_set": dict(
            blue="blue",
            a=4,
            your_mom="beer",
            added_child_set=dict(blue="blue", a=4, your_mom="beer"),
        ),
    }
    assert param_set._convert_to_dict_tree() == param_set_as_dict


def test_activity_documentation_with_caosdb():
    try:
        import caosdb  # noqa: 401
    except ModuleNotFoundError:
        raise ModuleNotFoundError("Not testing with CaosDB. Module not found.")
    output_dir = "./unittests/test_output/with_caosdb/"
    _clean_output_dir(output_dir)
    archive = gsp.ActivityDocumentation(
        activity_description="This is a test for the activity documentation class",
        activity_output_folder=output_dir,
        overwrite_output_folder=True,
        use_caosdb_parameter_storage=True,
        caosdb_root_record_name="MonodomainTissueSimulation",
    )
    fp = "./unittests/TestInput/arr.npy"
    arr = archive.inputs.log_and_load(fp, np.load, fp)

    archive.parameters.add_parameters(blue="blue", a=4, your_mom="beer")
    added_set1 = archive.parameters.create_and_add_parameter_set(
        "added_set", "MonodomainTissueSimulation", red="red", c=6, your_mom="spirit"
    )
    added_set1.create_and_add_parameter_set(
        "added_child_set",
        "MonodomainTissueSimulation",
        purple="purple",
        b=5,
        your_mom="whine",
    )
    archive.outputs.add_output("arr.npy", lambda fp: np.save(fp, arr))
    archive.single_value_results.add(cleopatra="ancient egypt")

    archive.write_documentation()
    assert os.path.isfile("./unittests/test_output/with_caosdb/Inputs.txt")
    assert os.path.isfile("./unittests/test_output/with_caosdb/Parameters.yaml")
    assert os.path.isfile("./unittests/test_output/with_caosdb/Readme.md")
    assert os.path.isfile("./unittests/test_output/with_caosdb/SingleValueResults.yaml")
    assert os.path.isfile("./unittests/test_output/with_caosdb/arr.npy")
    assert os.path.isfile("./unittests/test_output/with_caosdb/environment.yaml")
    assert os.path.isfile("./unittests/test_output/with_caosdb/output.info")


def _clean_output_dir(path_to_dir):
    for file_ in [
        "Inputs.txt",
        "Parameters.yaml",
        "Readme.md",
        "SingleValueResults.yaml",
        "arr.npy",
        "environment.yaml",
        "output.info",
    ]:
        try:
            os.remove(os.path.join(path_to_dir, file_))
        except FileNotFoundError:
            pass
    if os.path.isdir(path_to_dir) and os.listdir(path_to_dir):
        raise RuntimeError(
            f"{path_to_dir} should be empty but isn't."
            " Still in directory:"
            ", ".join(os.listdir(path_to_dir))
        )


def _setup_nocaosdb_parameter_set():
    root_set = ParameterSet(blue="blue", a=4, your_mom="beer")
    child1 = root_set("added_set", ParameterSet(blue="blue", a=4, your_mom="beer"))
    child1("added_child_set", ParameterSet(blue="blue", a=4, your_mom="beer"))
    return root_set


def _setup_caosdb_record(empty_record=False):
    try:
        from caosdb.high_level_api import create_record  # noqa: 401
    except ModuleNotFoundError:
        raise ModuleNotFoundError("Not testing with CaosDB. Module not found.")
    if empty_record:
        return create_record("MonodomainTissueSimulation")
    r = create_record("MonodomainTissueSimulation", blue="blue", a=4, your_mom="beer")
    r.added_set = create_record(
        "MonodomainTissueSimulation", red="red", c=6, your_mom="spirit"
    )
    r.added_set.added_child_set = create_record(
        "MonodomainTissueSimulation", purple="purple", b=5, your_mom="whine"
    )
    return r
