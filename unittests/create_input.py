import numpy as np
import os


def main():
    rng = np.random.default_rng(seed=0)
    arr = rng.random(size=(10000, 40))
    out_folder = "./TestInput"
    os.makedirs(out_folder, exist_ok=True)
    np.save(os.path.join(out_folder, "arr.npy"), arr)


if __name__ == "__main__":
    main()
