from GoodScientificPractice import utilities as uts
import yaml


class OutputLogger(object):
    def __init__(self, output_folder, create_folder=True, overwrite=False):
        """Stores files to output
        Args:
            - output_folder: the folder where the outputs are stored
            - create_folder: bool, whether to create the folder if not present
            - overwrite: whether to overwrite the files in the output folder
                if they already exist

        Notes:
            - If an output is already marked for storing and a new file with the
            same filepath will be marked for storing, the first will be overwritten.

        """

        if overwrite is False and uts.path_exists(output_folder):
            raise RuntimeError(
                f"Path {output_folder} exists and overwrite is False."
                " Cannot log with this folder."
            )
        self._output_log_file = uts.pathjoin(output_folder, "output.info")
        if overwrite and uts.path_exists(self._output_log_file):
            uts.remove_file(self._output_log_file)

        if create_folder:
            uts.make_dirs(output_folder, exist_ok=overwrite)

        self.output_folder = output_folder
        self._outputs = {}

    def add_output(self, filename, saving_function, to_output_dir=True):
        """Add an object to outputs.
        Args:
            - filename: Identifier for the output log. Usually the file path is a good
            choice
            - saving_function: the function used to save the object. Has to
            be of signature saving_function(filename). Use lambda functions
            if more is required, see usage.
            - to_output_dir: If false, save only to filename, otherwise
            will save to output dir.

        Usage:
            todo
        """
        filename = (
            uts.pathjoin(self.output_folder, filename) if to_output_dir else filename
        )
        self._outputs[filename] = saving_function

    def save_current(self, print_info=True, print_abspath=True):
        """Saved what is currently stored. Whatever is saved will be removed
        from this class.
        """

        saved_hashes = {}
        for filename, saver in self._outputs.items():
            saver(filename)
            saved_hashes[filename] = uts.file_sha256_hash(filename)
            _print_output_info(filename, print_info, print_abspath)

        if not uts.is_file(self._output_log_file):
            create_output_info(self._output_log_file)
        uts.write_to_file(self._output_log_file, "a", yaml.dump(saved_hashes))
        _print_output_info(self._output_log_file, print_info, print_abspath)
        self._outputs = {}


def _print_output_info(filename, print_info, print_abspath):
    """Print info about saved file.
    Args:
        - filename: the filename
        - print_info: whether this info shall be printer
        - print_abspath: if you the absolute path or not
    """
    if print_info:
        print(f"Saved {uts.as_abspath(filename) if print_abspath else filename}.")


def create_output_info(filepath):
    """Create the output info file.
    Args:
        - filepath: path to the output info file
    Returns:
        -
    Notes:
        - overwrites always
    """
    header = "# Output log for this folder\n#filepath: sha256 hash\n"
    uts.write_to_file(filepath, "w", header)
