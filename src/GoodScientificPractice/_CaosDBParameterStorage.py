"""
This is all a hack to make use of caosdb possible
"""
import yaml
from caosdb.high_level_api import create_record, CaosDBPythonRecord, CaosDBPythonEntity


def caosdb_parameter_record(caosdb_record_type, caosdb_name="", **kwargs):
    record = create_record(caosdb_record_type, caosdb_name, **kwargs)
    return record


def _call_(self, param_name, param_value):
    """Store the parameter given with its value.
    Args:
        - param_name: the parameter name. the parameter will
        be accessible via self.param_name (if name provided
        allows this python syntax)
        - param_value: the parameter value
    Returns:
        - param_value: The parameter value
    """
    self.__setattr__(param_name, param_value)
    return param_value


def _add_parameters(self, **kwargs):
    """Add multiple parameter at once and return them.
    Args:
        **kwargs: keyword arguments of form
        parameter_name = parameter_value.
    Returns:
        kwargs: the parameter_name: parameter_value
        dictionary as given in kwargs.
    """
    for param_name, param_value in kwargs.items():
        self(param_name, param_value)
    return kwargs


def _add_parameter(self, name, value):
    """Add one parameter and return it's value.
    Args:
        - name: the parameter name
        - value: the parameter value
    Returns:
        - value: the parameter value
    """
    return self(name, value)


def construct_from_yaml(self, path_to_yaml):
    """Constructs this class from a yaml file that has been
    saved from a child of this class.
    Args:
        - path_to_yaml: path to yaml file
    Returns:
        - a new parameter set based on the yaml file
    """

    with open(path_to_yaml, "r") as f:
        record = CaosDBPythonEntity.deserialize(yaml.safe_load(f))
    return record


def _is_part_of_collection(self, parameter_collection):
    """Check if this instance is parameter_collection or among
    the attributes of parameter_collection.
    Args:
        - parameter_collection: a parameter collection
    Returns:
        - bool: True if part of collection, False otherwise
    """
    if self is parameter_collection:
        return True

    child_parameter_sets = tuple(
        filter(
            lambda attr: isinstance(attr, type(self)),
            parameter_collection.__dict__.values(),
        )
    )
    if len(child_parameter_sets) == 0:
        return False
    else:
        return any((self._is_part_of_collection(cps) for cps in child_parameter_sets))


CaosDBPythonRecord.__call__ = _call_
CaosDBPythonRecord.add_parameters = _add_parameters
CaosDBPythonRecord.add_parameter = _add_parameter
CaosDBPythonRecord._is_part_of_collection = _is_part_of_collection
CaosDBPythonRecord.construct_from_yaml = construct_from_yaml
