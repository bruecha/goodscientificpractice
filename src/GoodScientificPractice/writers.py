def figure_saver(fig, *args, **figure_savekwargs):
    """Return a function that saves a matplotlib figure. This function that can be called
    with the filename only because all the additional kwargs have been given with call
    of this function. See example below. Do not give the figure filename to this
    function.

    Args:
        - fig: the figure to save
        - figure_savekwargs: keyword

    Returns:
        - function to save fig.

    Usage:

        >>> import GoodScientificPractice as gsp
        >>> outputs = gsp.Outputs("/my/output/folder")
        >>> fig = visualisation.create_figure()
        >>> saver = gsp.writers.figure_saver(fig, bbox_inches='tight',...)
        >>> outputs.add_output("my_figure", saver)


    To save the figure `fig` with the additional keyword argument
    'bbox_inches' = 'tight' as an example how to specify additional
    keyword arguments.
    Note that this saver can then only be used for the figure
    used at creation -- for another figure call this function with
    the other figure instance.
    """

    def save_fig(filename):
        """Save the figure attached at creation under filename.
        Args:
            - filename
        """
        fig.savefig(filename, **figure_savekwargs)

    if len(args):
        raise RuntimeError(
            "Do not give arguments besided the"
            "figure instance to this function."
            " Keyword arguments to the figure have to be given "
            "as keyword arguments to this funciton"
        )

    return save_fig


def obj_as_stringrep(obj):
    """Saves the objects using its string representation.
    Args:
        - obj: the object
    Returns:
        - A function of signature fun(filepath) that saves
        the object provided using its string representation
        to filepath.

    Usage:

        >>> import GoodScientificPractice as gsp
        >>> outputs = gsp.Outputs("/my/output/folder")
        >>> my_obj = caosdb.create_record("MySimulationRecord", responsible="Me")
        >>> string_saver = gsp.writers.string_rep_saver(my_obj)
        >>> outputs.add_output("Experiment_record.yaml", saver)

    """

    def write_string_rep(filepath):
        """Writes the string representation of obj given
        at creation.
        Args:
            - filepath: the filepath to save the object.
        """
        with open(filepath, "w") as f:
            f.write(str(obj))

    return write_string_rep


def numpy_saver(arr, *args, **array_savekwargs):
    """Return a function that saves a numpy array. This function that can be called
    with the filename only because all the additional kwargs have been given with call
    of this function. See example below. Do not give the filename to this
    function.

    Args:
        - arr: the figure to save
        - array_savekwargs: keyword arguments

    Returns:
        - function to save the array of signature fun(filepath).

    Usage:

    """
    import numpy as np

    def save_arr(filename):
        """Save the figure attached at creation under filename.
        Args:
            - filename
        """
        np.save(filename, arr, **array_savekwargs)

    if len(args):
        raise RuntimeError(
            "Do not give arguments besided the"
            "arr instance to this function."
            " Keyword arguments to the arr have to be given "
            "as keyword arguments to this funciton"
        )

    return save_arr
