import os
import sys
import subprocess
import yaml
import hashlib


def pathjoin(*paths):
    return os.path.join(*paths)


def make_dirs(path, exist_ok):
    os.makedirs(path, exist_ok=exist_ok)


def as_abspath(path):
    return os.path.abspath(path)


def is_file(path):
    return os.path.isfile(path)


def path_exists(path):
    return os.path.exists(path)


def yaml_dump(obj):
    return yaml.dump(obj)


def file_sha256_hash(filepath):
    """Return the sha256 hash of the file
    Args:
        - filepath: the filepath
    Returns:
        - sha256 hash of the file


    Notes:
        Found at:
    https://www.quickprogrammingtips.com/python/how-to-calculate-sha256-hash-of-a-file-in-python.html
    """
    sha256_hash = hashlib.sha256()
    with open(filepath, "rb") as f:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)

    return sha256_hash.hexdigest()


def create_file(path, mode):
    with open(path, mode=mode):
        pass
    return path


def remove_file(path):
    os.remove(path)


def get_environment():
    environment = {}
    environment["shell_env"] = subprocess.run(
        "printenv", stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
    ).stdout
    environment["pip_freeze"] = subprocess.run(
        ["pip", "freeze"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
    ).stdout
    return yaml_dump(environment)


def write_to_file(path, mode, str_):
    """Write content in str_ to file stored by path.
    Args:
        - path: path to the file that should be written
        - mode: the file opening mode (use google for more info)
        - str_ the string to write
    Returns:
        -
    """
    with open(path, mode=mode) as f:
        f.write(str_)


def create_READMEmd(
    responsible=None,
    description="No description provided",
    result_paths=None,
    input_paths=None,
):
    responsible = responsible if responsible else os.getlogin()
    result_paths = result_paths if result_paths else []
    input_paths = input_paths if input_paths else []

    readme = "\n".join(
        [
            "---",
            "responsible: ",
            f"  - {responsible}",
            f"description: {description}",
            f"script: {os.path.abspath(sys.argv[0])}",
            "",
        ]
    )
    if result_paths:
        readme = readme + "\nresults:\n" + yaml_dump(result_paths)

    if input_paths:
        readme = readme + "\nsources:\n" + yaml_dump(input_paths)
    readme = readme + "\n ...\n\n\nNotes:\n\n"
    readme = readme + "# Place for notes\n\n\n"
    return readme
