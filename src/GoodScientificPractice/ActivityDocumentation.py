from GoodScientificPractice.Inputs import InputLogger as _InputLogger
from GoodScientificPractice.Outputs import OutputLogger as _OutputLogger
from GoodScientificPractice.Parameters import ParameterSet as _ParameterLogger
from GoodScientificPractice.SingleValueStorage import SingleValueStorer
from GoodScientificPractice import writers as _writers
from GoodScientificPractice import utilities as uts

try:
    from GoodScientificPractice._CaosDBParameterStorage import caosdb_parameter_record
except ModuleNotFoundError:
    pass


class ActivityDocumentation(object):
    def __init__(
        self,
        *args,
        responsible=None,
        activity_description="",
        activity_output_folder="path/to/folder",
        overwrite_output_folder=False,
        use_caosdb_parameter_storage=False,
        caosdb_root_record_name="",
        caosdb_realisation_name=""
    ):
        """
        Args:
            [...] other args not yet documented
            - get_parameter_storage: Function to create a ParameterStorage object.
            The object will be accessible via self.parameters.
            To use the functionality provided by this class, either the object
            has to be callable by parameter_storage(name, value), which will add
            the value as an attribute with name name to the storage.
            This keyword was added to support
        """
        if len(args):
            raise RuntimeError("Only keyword arguments allowed")
        self._responsible = responsible
        self._description = activity_description

        # outputs has to be created first as the others will
        # be marked as output
        self.outputs = _OutputLogger(
            activity_output_folder, overwrite=overwrite_output_folder
        )
        self.parameters = _create_root_parameterset(
            use_caosdb_parameter_storage,
            caosdb_root_record_name,
            caosdb_realisation_name,
        )

        self.inputs = _InputLogger()
        self.single_value_results = SingleValueStorer()
        self.outputs.add_output(
            "environment.yaml", _writers.obj_as_stringrep(uts.get_environment())
        )

    def parameters_from_yaml(self, path_to_file):
        """Load parameters from yaml file."""
        self.parameters = self.parameters.construct_from_yaml(path_to_file)

    @property
    def parameters(self):
        """Get or set the parameter storage class. It is recommended to use
        the built-in classes, either with caosdb storage or without as can
        be set in the initialisation process of this class.

        Setting this property automatically writes a string-representation
        of the parameter class in the *this instance*.write_documentation()
        process.
        """
        return self._parameters

    @parameters.setter
    def parameters(self, parameter_storage):
        self._parameters = parameter_storage
        self.outputs.add_output(
            "Parameters.yaml", _writers.obj_as_stringrep(self._parameters)
        )

    @property
    def inputs(self):
        """Input logger. It is recommended to use the built-in input-logger.

        Setting this property automatically writes a string-representation
        of the input class in the *this instance*.write_documentation()
        process.
        """
        return self._input_logger

    @inputs.setter
    def inputs(self, input_logger):
        self._input_logger = input_logger
        self.outputs.add_output(
            "Inputs.txt", _writers.obj_as_stringrep(self._input_logger)
        )

    @property
    def outputs(self):
        """Output logger. It can log output and write it either inbetween or as
        part of the *this instance*.write_documentation() process.
        """
        return self._output_logger

    @outputs.setter
    def outputs(self, output_logger):
        self._output_logger = output_logger

    @property
    def single_value_results(self):
        """Convenience class to store single value results in a .yaml file
        in name-value pairs. If single values are stored, they will be
        written as part of the *this instance*.write_documentation() process.
        """
        return self._single_value_results

    @single_value_results.setter
    def single_value_results(self, single_value_results):
        self._single_value_results = single_value_results

    def write_documentation(self, print_info=True, print_abspath=True):
        if len(self.single_value_results._value_storage):
            self.outputs.add_output(
                "SingleValueResults.yaml",
                _writers.obj_as_stringrep(self.single_value_results),
            )
        self.outputs.add_output(
            "Readme.md",
            _writers.obj_as_stringrep(
                uts.create_READMEmd(
                    responsible=self._responsible,
                    description=self._description,
                    result_paths=list(self._output_logger._outputs.keys()),
                    input_paths=list(self._input_logger._logged_inputs.keys()),
                )
            ),
        )
        self._output_logger.save_current(
            print_info=print_info, print_abspath=print_abspath
        )


def _create_parameter_set(self, **kwargs):
    """Create new parameter set

    Args:
        - **kwargs: will be added to the new parameter set as parameter_name
          parameter-value pairs

    Returns: The newly created parameter_set

    """

    parameter_set = _ParameterLogger(**kwargs)
    return parameter_set


def _create_and_add_parameter_set(self, name, *args, **kwargs):
    """

    Args:
        - name: name of this parameter set
        - **kwargs: will be added to the new parameter set as parameter_name
          parameter-value pairs

    Returns: The newly created parameter_set

    """
    if len(args):
        raise RuntimeError("No args beside name are allowed.")
    parameter_set = _ParameterLogger(**kwargs)
    self.add_parameters(**{name: parameter_set})
    return parameter_set


def _create_parameter_set_caosdb(self, caosdb_record_name, *args, **kwargs):
    """Create a caosdb record based on caosdb_record_name
    Args:
        - caosdb_record_name: name of the CaosDB record type
        - **kwargs: will be added to the new parameter set as parameter_name
          parameter-value pairs

    Returns: The newly created parameter_set
    """
    if len(args):
        raise RuntimeError("No args beside name are allowed.")
    parameter_set = caosdb_parameter_record(caosdb_record_name, **kwargs)
    return parameter_set


def _create_and_add_parameter_set_caosdb(
    self, name, caosdb_record_name, *args, **kwargs
):
    """

    Args:
        - name: name in the parameter storage
        - caosdb_record_name: name of the CaosDB record type
        - **kwargs: will be added to the new parameter set as parameter_name
          parameter-value pairs

    Returns: The newly created parameter_set
    """
    if len(args):
        raise RuntimeError("No args beside name are allowed.")
    parameter_set = caosdb_parameter_record(caosdb_record_name, **kwargs)
    self.add_parameters(**{name: parameter_set})
    return parameter_set


def _create_root_parameterset(
    use_caosdb_parameter_storage, caosdb_root_record_name, caosdb_realisation_name
):
    if use_caosdb_parameter_storage:
        root_parameter_set = caosdb_parameter_record(
            caosdb_root_record_name, caosdb_realisation_name
        )
        type(root_parameter_set).create_parameter_set = _create_parameter_set_caosdb
        type(
            root_parameter_set
        ).create_and_add_parameter_set = _create_and_add_parameter_set_caosdb
    else:
        root_parameter_set = _ParameterLogger()
        type(root_parameter_set).create_parameter_set = _create_parameter_set
        type(
            root_parameter_set
        ).create_and_add_parameter_set = _create_and_add_parameter_set
    return root_parameter_set
