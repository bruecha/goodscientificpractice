import yaml


class ParameterSet:
    def __init__(self, **kwargs):
        for arg_name, val in kwargs.items():
            self.__setattr__(arg_name, val)

    def __str__(self, level=0):
        return yaml.dump(self._convert_to_dict_tree())

    def _convert_to_dict_tree(
        self,
    ):
        as_dict = {}
        for key, value in self.__dict__.items():
            if isinstance(value, ParameterSet):
                as_dict[key] = value._convert_to_dict_tree()
            else:
                as_dict[key] = value
        return as_dict

    def __call__(self, param_name, param_value):
        """Store the parameter given with its value.
        Args:
            - param_name: the parameter name. the parameter will
            be accessible via self.param_name (if name provided
            allows this python syntax)
            - param_value: the parameter value
        Returns:
            - param_value: The parameter value
        """
        self.__setattr__(param_name, param_value)
        return param_value

    def add_parameters(self, **kwargs):
        """Add multiple parameter at once and return them.
        Args:
            **kwargs: keyword arguments of form
            parameter_name = parameter_value.
        Returns:
            kwargs: the parameter_name: parameter_value
            dictionary as given in kwargs.
        """
        for param_name, param_value in kwargs.items():
            self(param_name, param_value)
        return kwargs

    def add_parameter(self, name, value):
        """Add one parameter and return it's value.
        Args:
            - name: the parameter name
            - value: the parameter value
        Returns:
            - value: the parameter value
        """
        return self(name, value)

    def construct_from_dict(self, dict_):
        """Construct a ParameterSet based on a dictionary.
        Args:
            - dict_: the dictionary to use for construction.
        Returns:
            - self (an instance of this class)
        Note:
            - subdictionaries will be converted to ParameterSet
            instances
        """
        for key, value in dict_.items():
            if isinstance(value, dict):
                param_set = ParameterSet()
                setattr(self, key, param_set.construct_from_dict(value))
            else:
                setattr(self, key, value)
        return self

    def construct_from_yaml(self, path_to_yaml):
        """Constructs this class from a yaml file that has been
        saved from a child of this class.
        Args:
            - path_to_yaml: path to yaml file
        Returns:
            - a new parameter set based on the yaml
        """
        with open(path_to_yaml, "r") as f:
            dict_ = yaml.safe_load(f)

        new_set = ParameterSet()
        return new_set.construct_from_dict(dict_)

    def _is_part_of_collection(self, parameter_collection):
        """Check if this instance is parameter_collection or among
        the attributes of parameter_collection.
        Args:
            - parameter_collection: a parameter collection
        Returns:
            - bool: True if part of collection, False otherwise
        """
        if self is parameter_collection:
            return True

        child_parameter_sets = tuple(
            filter(
                lambda attr: isinstance(attr, type(self)),
                parameter_collection.__dict__.values(),
            )
        )
        if len(child_parameter_sets) == 0:
            return False
        else:
            return any(
                (self._is_part_of_collection(cps) for cps in child_parameter_sets)
            )
