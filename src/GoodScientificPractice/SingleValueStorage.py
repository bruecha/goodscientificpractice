import yaml


class SingleValueStorer(object):
    """"""

    def __init__(
        self,
    ):
        """A convenience class to store single values in scientific activities.
        See examples.

        Example:

            Very often in scientific programming one has several single-value results
            or so and one wonders how to store them. This convenience class is made
            for this!

            >>> import numpy as np
            >>> arr=np.random.rand(400)
            >>> svs = SingleValueStorer()
            >>> std = svs.add_value(std, arr.std()) # or
            >>> std, m = svs.add_values(std=arr.std(), mean=arr.mean(), overwrite=True)
            >>> m, std = svs(mean=arr.mean(), std=arr.std(), overwrite=True)

            and svs will store the values for you! You can then write it with the
            OutputLogger or ActivityDocumentation class of this package!

        """
        self._value_storage = {}

    def add_value(self, name, value, overwrite=False):
        """Add a value to the storage
        Args:
            - name: parameter name
            - value: parameter value
            - overwrite: if parameter should be overwritten if exists.
            Default: False
        Returns:
            - value: the parameter value
        """
        if not overwrite and name in self._value_storage:
            raise RuntimeError(
                f"Parameter with name {name} already stored"
                "and overwrite is set to False"
            )
        self._value_storage[name] = value
        return value

    def add_values(self, overwrite=False, **kwargs):
        """Add values as name=value pairs to the storage.
        Args:
            - overwrite: if parameters should be overwritten if they exist.
            Default: False
            - kwargs: key-value pairs
        Returns:
            - list of the values given.
        """
        values = []
        for name, value in kwargs.items():
            self.add_value(name, value, overwrite=overwrite)
            values.append(value)
        return values

    def add(self, overwrite=False, **kwargs):
        """Add values as name=value pairs to the storage.
        Args:
            - overwrite: if parameters should be overwritten if they exist.
            Default: False
            - kwargs: key-value pairs
        Returns:
            - list of the values given.
        """
        return self.add_values(overwrite=overwrite, **kwargs)

    def __str__(self):
        return yaml.dump(self._value_storage)

    def __call__(self, overwrite=False, **kwargs):
        """Add values as name=value pairs to the storage.
        Args:
            - overwrite: if parameters should be overwritten if they exist.
            Default: False
            - kwargs: key-value pairs
        Returns:
            - list of the values given.
        """
        return self.add_values(overwrite=overwrite, **kwargs)
