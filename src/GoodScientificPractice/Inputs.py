import os
import yaml
from GoodScientificPractice.utilities import file_sha256_hash


class InputLogger(object):
    def __init__(
        self,
    ):
        """logs inputs"""
        self._logged_inputs = {}

    def log_and_load(
        self,
        filepath,
        loading_function,
        *loading_function_args,
        **loading_function_kwargs,
    ):
        """Add an object to outputs.
        Args:
            - filepath: the filepath
            - loading_function: the function used to load the file (don't call it!)
            - *loading_function_args: arguments for loading function as if you
            called loading function directly
            - ** loading_function_kwargs: keyword arguments for loading function
        Returns:
            - the return value from loading_function called with the provided args
              and kwargs.

        Note: Due to design reasons you have to give the filepath twice probably.

        Example:

            >>> filename = "./Inputs/example_file.npy"
            >>> arr = InputLogger.log_and_load(filename, np.load, filename)
            >>> print(arr) # returns the entries of the file loaded

        """

        self.log(filepath)
        return loading_function(*loading_function_args, **loading_function_kwargs)

    def log(self, path):
        """Logs a file as input without saving.
        Args:
            - path: The path of the file
        Returns:
            - path: the path as given
        """
        if not os.path.isfile(path):
            raise RuntimeError(f"{path} is not a file.")

        self._logged_inputs[os.path.abspath(path)] = str(file_sha256_hash(path))

        return path

    def __str__(self):
        header = "#filename: sha256_hash"
        return "\n".join((header, yaml.dump(self._logged_inputs)))
